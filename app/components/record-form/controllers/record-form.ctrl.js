

(function () {
   'use strict';
   
   angular.module('blog').controller('RecordFormCtrl',['$state', '$http', '$stateParams', 'configService', RecordFormCtrl]); 

    function RecordFormCtrl ($state, $http, $stateParams, configService) {
        var self = this;
        console.log($stateParams);
        self.name = $stateParams.name;
        if(typeof $stateParams.data === 'object'){
	        self.id = $stateParams.id;
	        self.author = $stateParams.data.Author;
			self.title =$stateParams.data.Title;
			self.text =$stateParams.data.Text;
        }
        self.entries = {};
        console.log(self.entry);  
        self.action = function (){
			    if(self.name === 'edit'){
			    	self.entries.Author = self.author;
					self.entries.Title = self.title;
					self.entries.Text = self.text;
					self.entries.EntryId = self.id;


			    	function success (response) {			        	
			        	self.entries = response.data;
			        }

				    function error (msg) {
				        	console.log(msg);
				    }

			  		$http.put(configService.host + '/entry', self.entries).then(success, error);
			  		console.log(self.entries);
			    }

			    if(self.name === 'add'){
			    	self.entries.Author = self.author;
					self.entries.Title = self.title;
					self.entries.Text = self.text;
			    	
					console.log(self.entries);

			    	function success (response) {
			        	// console.log(res.data);
			        	self.entries = response.data;
			        } 

				    function error (msg) {
				        	console.log(msg);
			        }

					$http.post(configService.host + '/entry',self.entries).then(success, error);
			    	
			    }		   
		}
    }

    
}());