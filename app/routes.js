
(function (){
   'use strict';
   
   angular.module('blog').config(routes);
   
   function routes ($stateProvider, $urlRouterProvider) {
       $urlRouterProvider.otherwise("/");
       $stateProvider 
       //Index
        .state('index', {
            url: '/',
            templateUrl: 'app/components/index/templates/index.tpl.html',
            controller: 'IndexCtrl',
            controllerAs: 'main'
        })
        //Record Form
        .state('index.form', {
            url: 'record/:name',
            templateUrl: 'app/components/record-form/templates/record-form.tpl.html',
            controller: 'RecordFormCtrl',
            controllerAs: 'form',
            params: {
                'data': 'data',
                'id':'id'
            }
        })
        //Record read    
        .state('index.read', {
            url: 'read-more',
            templateUrl: 'app/components/record/templates/record.tpl.html',
            controller: 'RecordCtrl',
            controllerAs: 'record',
            params: {
                'recordId': 'recordId'
            }
        })
        //Main Page       
        .state('index.blog', {
            url: 'my-blog',
            templateUrl: 'app/components/main-page/templates/main-page.tpl.html',
            controller: 'MainPageCtrl',
            controllerAs: 'myBlog'
        })
        

        
        // .state('index.blog.edit', {
        //     url: '/edit-record',
        //    templateUrl: 'app/components/record-edit/templates/record-edit.tpl.html',
        //     controller: 'RecordEditCtrl',
        //    controllerAs: 'edit'
        // })
       
//        .state('index.blog.delete', {
//           url: '/delete-record',
//            templateUrl: 'app/components/record-delete/templates/delete.tpl.html',
//           controller: 'DeleteCtrl',
//           controllerAs: 'delete'
//        })
        
        
;
   };
    
    
}());