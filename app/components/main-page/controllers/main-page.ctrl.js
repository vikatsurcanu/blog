/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


(function () {
   'use strict';
   
   angular.module('blog').controller('MainPageCtrl',['$state', '$http', 'configService', MainPageCtrl]); 

    function MainPageCtrl ($state, $http, configService) {
        var self = this;
        self.entries = [];
        self.readMore = function (id1) {
           console.log(id1);
           $state.go('index.read', {recordId: id1});
       }

       	function success (response) {
            	// console.log(res.data);
            	self.entries = response.data;
            }

        function error (msg) {
            	console.log(msg);
            }

      $http.get(configService.host + '/entry').then(success, error);

    }

    
}());