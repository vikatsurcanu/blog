(function () {
   'use strict';
   
   angular.module('blog').factory('configService',[configService]); 

    function configService() {
      var service = {};

      service.protocol = 'http';
      service.api = 'localhost:3333';
      service.host = service.protocol + '://' + service.api;

      return service;

      // var api;

      // api.get = function (str, s, e) {
      //   $http.get(str).then(s, e);
      // }
    };
    
}());