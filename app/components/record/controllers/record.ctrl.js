

(function () {
   'use strict';
   
   angular.module('blog').controller('RecordCtrl',['$state', '$http','$stateParams', 'configService', RecordCtrl]); 

    function RecordCtrl ($state, $http, $stateParams, configService) {
        var self = this;
        self.recordId = $stateParams.recordId;
        self.entries = {}; 
        self.DeleteRecord = function (id) {
		  	console.log('deleted');
		  	function success (response) {
		        self.entries = response.data;
		    }
			function error (msg) {
		        console.log(msg);
		    }

		  	$http.delete(configService.host + '/entry', {
		  		data:{"EntryId": id},
		  		headers:{
		  			'Content-Type':'application/json'
		  		}
		  	} ).then(success, error);		  	
		  	$state.go('index.blog');
		}
    	     
		if(self.recordId) {
		   	function success (response) {
		        	
		        	self.entries = response.data;
		        }

		    function error (msg) {
		        	console.log(msg);
		        }

		  	$http.get(configService.host + '/entry/'+self.recordId).then(success, error);		
		  
		}

		
}
    
}());